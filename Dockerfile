FROM registry.gitlab.com/servicelayers-public/bash-aws:2.0

USER 0

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
  apk add --update --no-cache etcd-ctl && \
  etcdctl --version

USER 1000

ARG IMAGE_COMMIT_ARG=""
ENV IMAGE_COMMIT=${IMAGE_COMMIT_ARG}
ARG IMAGE_TAG_ARG=""
ENV IMAGE_TAG=${IMAGE_TAG_ARG}
