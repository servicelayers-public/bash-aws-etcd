bash-aws-etcd
===

Alpine image with `bash`, `aws cli` and `etcdctl`, running as user `1000`, a member of group `0`.
